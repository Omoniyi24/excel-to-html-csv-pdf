# Excel - CSV - PDF Converter(PHP)

---
- [Summary](#summary)
- [Requirements](#requirements)
- [Setup](#setup)
- [Note](#note)

---
### Summary

This project parse excel file to CSV and PDF

---
### Requirements

* Ensure Composer is installed on your system.
* Web Server
* PHP 5.6.35 and above

---
### Setup


##### Install

In command line, navigate to the installation directory of this  project

Enter the following commands:

```
composer require dompdf/dompdf
```
After that run:

```
composer require phpoffice/phpspreadsheet
```


---
### Note

`downloading pdf takes quite some time due to the large size of the doc`
